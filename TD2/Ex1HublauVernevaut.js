"use strict"

/* exercice 1.1 */

let range = (a,b) => {
  if( a <= b) {
    let t = [];
    for (a; a <= b; a++) { t.push(a); }
      return t;
  } else {
    let t = [];
    for (b; b <= a; b++) { t.push(b); }
      return t;
  }
}

/* exercice 1.2 */

let sumv1 = (tab) => {
  let sum = 0;
  for (let e of tab) {
    sum += e;
  }
  return sum;
}

let sumv2 = (tab) => {
  let sum = 0;
  tab.forEach( (e) => { sum += e;} );
  return sum;
}

let sumv3 = (tab) => {
  let sum = (a,e) => {return a+e;};
  let res = tab.reduce(sum,0);
  return res;
}

/* exercice 1.3 */

let moyv1 = (tab) => {
  let sum = 0;
  for (let e of tab) {
    sum += e;
  }
  return sum/tab.length;
}

let moyv2 = (tab) => {
  let sum = 0;
  tab.forEach( (e) => { sum += e;} );
  return sum/tab.length;
}

let moyv3 = (tab) => {
  let sum = (a,e) => {return a+e;};
  let res = tab.reduce(sum,0);
  return res/tab.length;
}

let moyvspe = (tab) => {
  let sum = sumv1(tab);
  return sum/tab.length;
}

/* exercice 1.4 */

let tabchaine = (t,pattern) => {
  let res = [];
  t.forEach( (e) => {
    if(e.match(pattern))
      res.push(e.toUpperCase()); });
  return res;
}

/* exercice 1.5 */

let testFnct = (pattern) => {
  return (tableau) => {
    let res = [];

    tableau.forEach( (e) => {
      if (e.match(pattern))
        res.push(transFnct(e));
    });

    return res;
  };
}

let transFnct = (elem) => {
  return elem.toUpperCase();
}

let filter = (t,testFnct,transFnct) => {
  return testFnct(t);
}

let tabchainegene = (t, p) => {
  return filter(t,testFnct(p),transFnct);
}

/* exercice 1.6 */

let tabfiltre = (t,p) => {
  let trouve = (e) => { return e.match(p); };
  let modif = (e) => { return e.toUpperCase(); };
  let res = t.filter(trouve).map(modif);
  return res;
}

/* exercice 1.7 */

let functurl = (tab) => {
  let url = (e) => { return "http://www.cata.log/products/"+e+"\n";};
  return tab.map(url);
}

/* exercice 1.8 */

let functlien = (tab) => {
  let a = (e) => { return "<li> <a href=\"url\"> "+e+"</a></li>\n";};
  return tab.map(a);
}
