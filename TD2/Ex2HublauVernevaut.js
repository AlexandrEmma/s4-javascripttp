"use strict"

/* exercice 2.1 */

let objectfct = (tab) => {
  let somme = 0;
  tab.forEach((e)=>somme+=e);
  let tableau = {
    nbelem : tab.length,
    sommelem : somme,
    moyennelem : somme/tab.length
  } ;
  return tableau;
}

/* exercice 2.2 2.3 2.4 */

let etudiant = {
  numero : 1,
  nom : "Hublau",
  prenom : "Alexandre",
  dateNaiss : new Date('1999-2-1T03:24:00'),
  mail : "contact@alexandre-hublau.com",
  notes : [],

  age() {
    let anneenow = new Date();
		let ageDate = this.dateNaiss;
    if(anneenow.getMonth()-ageDate.getMonth() < 0) {
		    return (anneenow.getFullYear()-ageDate.getFullYear())-1;
    } else {
        return (anneenow.getFullYear()-ageDate.getFullYear());
    }
  },

  afficher() {
    return (this.nom.toUpperCase() + " " + this.prenom + " " + this.dateNaiss.getDate() + "/" +
            this.dateNaiss.getMonth() + "/" + this.dateNaiss.getFullYear());
  },

  ajouterNote(m,n) {
    let NoteMatiere = {
      matiere : m,
      note : n
    };
    this.notes.push(NoteMatiere);
  },

  calculerMoyenne() {
    let somme = 0;
    this.notes.forEach( (e) => somme += e.note);
    return somme/this.notes.length;
  }

}

/* exercice 2.6 */

function etud(num,nom,pren,dateN,mail) {
  this.numero = num;
  this.nom = nom;
  this.prenom = pren;
  this.dateNaiss = new Date(dateN);
  this.mail = mail;
  this.notes = [];
}

etud.prototype.age = function () {
  let anneenow = new Date();
  let ageDate = this.dateNaiss;
  if(anneenow.getMonth()-ageDate.getMonth() < 0) {
      return (anneenow.getFullYear()-ageDate.getFullYear())-1;
  } else {
      return (anneenow.getFullYear()-ageDate.getFullYear());
  }
}

etud.prototype.afficher = function () {
  return (this.nom.toUpperCase() + " " + this.prenom + " " + this.dateNaiss.getDate() + "/" +
          this.dateNaiss.getMonth() + "/" + this.dateNaiss.getFullYear());
}

etud.prototype.ajouterNote = function (m,n) {
  let NoteMatiere = {
    matiere : m,
    note : n
  };
  this.notes.push(NoteMatiere);
}

etud.prototype.calculerMoyenne = function () {
  let somme = 0;
  this.notes.forEach( (e) => somme += e.note);
  return somme/this.notes.length;
}

/* exercice 2.7 */

let etuanniv = (tab,mois) => {
  let res = [];
  tab.forEach( (e) => {
    if(e.dateNaiss.getMonth() == mois) {
      res.push(e);
    }
  });
  return res;
}

/* exercice 2.8 */

let etuplusage = (tab,age) => {
  let ageplusgrand = (e) => { return age<=e.age(); };
  return tab.filter(ageplusgrand);
}

/* exercice 2.9 2.10 2.11*/

function groupetu (n,f,a) {
  this.nom = n;
  this.formation = f;
  this.liste = [];
  this.annee = new Date(a);
}

groupetu.prototype.ajouterEtu = function (e) {
  if(e != null) this.liste.push(e);
}

groupetu.prototype.compteretu = function () {
  return this.liste.length;
}

groupetu.prototype.calculmoy = function () {
  let somme = 0;
  this.liste.forEach((e) => { if(e.notes.length != 0) somme += e.calculerMoyenne() } );
  return somme/this.liste.length;
}

groupetu.prototype.joyeuxanniv = function (mois,matiere) {
  let fil = (e) => { if(e.dateNaiss.getMonth() == mois)  return true };
  let mp = (e) => {  e.notes.forEach((e2) => {if(e2.matiere == matiere) e2.note += 2 }) };
  this.liste.filter(fil).map(mp);
}
