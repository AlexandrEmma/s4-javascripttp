# Notes pour le professeur

## Erreur **Blocked by CORS policy**

Si cette erreur apparait, il faut:

* Lancer l'application sur **localhost**

	php -S localhost:8080

* Se rendre sur l'URL **localhost:8080**

* Sur Chrome, il y a une extension **CORS Enabler**, il suffit de l'installer

(https://chrome.google.com/webstore/detail/cors-enabler/moemeipfappdfiffdphjhhknpmdpjffp)[https://chrome.google.com/webstore/detail/cors-enabler/moemeipfappdfiffdphjhhknpmdpjffp]


# Notes:

* Corentin: Faire fonctions privée

# Partie 2

Exercice 2: Facile
Exercice 3: Facile
