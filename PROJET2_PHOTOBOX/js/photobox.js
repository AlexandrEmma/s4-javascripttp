//module principal

import gallery from './gallery.js'
import lightbox from './lightbox.js'


$(document).ready(function() {

	// PARTIE 1, Q1
	gallery.init()

	//gallery.load()
	$('#load_gallery').click( _ => gallery.load() )

	//================================================================

	// PARTIE 1, Q2
	lightbox.addEventsToVignettes()

	//================================================================

	// PARTIE 1, Q3
	$('#previous').click( _ => gallery.previousPage() )
	$('#next').click( _ => gallery.nextPage() )

	//================================================================

	// PARTIE 2, Q1
	$('#p').click( _ => gallery.previousBox() )
	$('#n').click( _ => gallery.nextBox() )

})
