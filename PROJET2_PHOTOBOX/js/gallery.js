import photoloader from './photoloader.js'
import lightbox from './lightbox.js'

let gallery = (function() {

	/**
	 * Initialise le module
	 */
	 let init = function() {
	 	this.pageNumber = 1
	 	this.numberPerPage = 12
	 	photoloader.init("https://webetu.iutnc.univ-lorraine.fr")
	 	this.boxLink = new Map()
	 }

	/**
	 * Execute une requete AJAX et appelle la fonction qui met a jour le DOM
	 */
	 let load = function() {

		// Execute requete AJAX et charge le DOM
		photoloader.load("/www/canals5/photobox/photos/?offset=" + ((this.pageNumber*this.numberPerPage) - this.numberPerPage) + "&size=" + this.numberPerPage)

		.then(json => json.photos)

		.then(photos => new Promise( (resolve, reject) => {

			if(photos.length === 0) {
				this.pageNumber --
				reject('Erreur promesse: nombre de photo')
			}

			return resolve(photos)
		}))

		.then(photos => this.generateDom(photos))
		.catch(err => console.log(err))

	}

	/**
	 * Charge la page precedente
	 */
	 let previousPage = function() {
		// Ne peut pas être < 1
		if(this.pageNumber > 1)
			this.pageNumber--

		this.load()
	}

	/**
	 * Charge la page suivante
	 */
	 let nextPage = function() {
		this.pageNumber++ // (La page max est definie dans le dom)
		this.load()
	}


	let previousBox = function() {
		console.log('TODO, prev')
	}

	let nextBox = function() {
		console.log('TODO, next')
	}
	/**
	 * Genere le DOM
	 * @param  {ajax response}	response	Donnee recuperee
	 */
	let generateDom = function(photos) {

		// On ajoute les photos
		// Se passe en 2 étapes:
		// 		Remplace les photos déjà existantes
		// 		Ajoutes les photos qu'il manque
		let photoInTheDom = $('.vignette').length
		photos.forEach((photo, i) => {
			let image_link_large = photo.photo.original.href
			let image_link_small = photo.photo.thumbnail.href
			let name_image = photo.photo.file

			// Mofifie les div existantes
			let el = $('#photobox-gallery').children('.vignette')[i]

			let image = $($(el).children('img').first())
			image.attr('data-img', `${photoloader.server_url}${image_link_large}`)
			image.attr('src', `${photoloader.server_url}${image_link_small}`)


			// Ajoute celles qui manquent
			// Si dans le code HTML il y a que 5 photos, alors on modifie les 5 (dans le block)
			//  au dessus, et on ajoute les 7 qui manquent
			if(photoInTheDom <= i) {
				$('#photobox-gallery').append(`<div class="vignette">
					<img data-img="${photoloader.server_url}${image_link_large}"
					src="${photoloader.server_url}${image_link_small}">
					<div>${name_image}</div>
					</div>`)

				lightbox.addEventsToVignettes()
			}

			this.boxLink.set(i, image_link_large) // Ajout lien dans box

		})





	}

	return { init, load, previous, next, generateDom, previousPage, nextPage, previousBox, nextBox }

})()

export default {
	init: gallery.init,
	load: gallery.load,
	previous: gallery.previous,
	next: gallery.next,
    generateDom: gallery.generateDom,
    previousPage: gallery.previousPage,
    nextPage: gallery.nextPage,
    previousBox: gallery.previousBox,
    nextBox: gallery.nextBox
}
