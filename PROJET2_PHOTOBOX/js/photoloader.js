
let photoloader = (function() {

	/**
	 * Initialise le module
	 * @param  {string} server_url URL du serveur
	 */
	 let init = function(server_url) {
	 	this.server_url = server_url
	 }

	/**
	 * Renvoie une promesse contenant la requete AJAX
	 * @param  {string} uri URI a appeler
	 * @return {Primise}     Promesse contenant la requete
	 */
	 let load = function(uri) {

	 	return axios.get(this.server_url + uri)
	 	.then(response => response.data)
	 	.catch(err => 'Erreur Axios')

	 	/*
	 	return axios.request({
	 		url : this.server_url + uri,
	 		method : 'get'
	 	})


		return new Promise((resolve, reject) => {
			let req = new XMLHttpRequest()
			req.open('GET', this.server_url + uri, true)

			req.onreadystatechange = function (aEvt) {
				if (req.readyState == 4) {
					if(req.status == 200)
						resolve(req.responseText)
					else
						reject(req)
				}
			}
			req.send(null)

		})
		*/
	}


	return { init, load }

})()

export default {
	init: photoloader.init,
	load: photoloader.load
}
