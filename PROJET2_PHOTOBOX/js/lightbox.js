import photoloader from './photoloader.js'

let lightbox = (function() {


	let createLightbox = function(vignette) {

		// Mise a jour photo
		let img_large_url = $(vignette).children('img').first().attr('data-img')
		$("#lightbox_full_img").attr("src", img_large_url)

		// Mise a jour titre
		let titre = $(vignette).children('div').first().html()
		$("#lightbox_title").html(titre)

		// Mise à jour detail

	}

	let displayLightBox = function() {
		$('#lightbox_container').css('display', "block")
	}

	return {
		/**
		 * Crée et affiche la lightbox
		 * @param  {DOM} vignette La vignette que l'on veut créer
		 */
		 createAndDisplay(vignette) {
		 	createLightbox(vignette)

		 	$("*").css("cursor","wait")
		 	$("#lightbox_full_img").on("load", _ => {
		 		displayLightBox()
		 		$("*").css("cursor","")
		 	})

		 },
		 removeLightbox() {
		 	$('#lightbox_container').css('display', "none")
		 },
		 addEventsToVignettes() {
		 	$('.vignette').click(event => lightbox.createAndDisplay(event.currentTarget) )
		 	$('#lightbox_close').click(_ => lightbox.removeLightbox() )
		 }
		}

	})()

	export default {
		init: lightbox.init,
		createAndDisplay: lightbox.createAndDisplay,
		removeLightbox: lightbox.removeLightbox,
		addEventsToVignettes: lightbox.addEventsToVignettes,
	}
