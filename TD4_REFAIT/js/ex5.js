"use strict"

export function init() {
  $("#four").on("click", () => {affichermodal("#modal1")});
  $(".modal_close").on("click",desaffichermodal);
}

function affichermodal(modal) {
  $(modal).css('display', 'block');
}

function desaffichermodal(e) {
  $(e.target).parent().parent().css('display', 'none');
}
