"use strict"

export function init() {
  $('#square > div').on("mouseover", changementTaille);

  $('.boite_orange').on("click", reinitTaille);

  $('#one').on("click", nouveldiv);

  $('#two').on("click", changement);

  $('#three').on("click", fade);
}


function changementTaille(e) {
  $(e.target).removeClass('boite_orange').addClass('boite_orange_survol');
}

function reinitTaille(e) {
  $(e.target).removeClass('boite_orange_survol').addClass('boite_orange');
}

function nouveldiv(e) {
  let ancienneval = $('#square > div:last-child').text();
  $('#square').append('\n   <div class="boite_orange">'+ (parseInt(ancienneval)+1) +'</div>');
  $('#square > div').on("mouseover", changementTaille);
  $('.boite_orange').on("click", reinitTaille);
}

function changement(e) {
  $('#square > div').toggleClass('boite_orange').toggleClass('boite_verte');
}

function fade(e) {
  $('#square > div').fadeToggle();
}
