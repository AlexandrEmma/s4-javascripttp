"use strict"


export function init() {
  $('#changingText').on("click", changerCouleurTexte);
}


function changerCouleurTexte(e) {
  $(e.target).toggleClass('zap').toggleClass('hop');
}
