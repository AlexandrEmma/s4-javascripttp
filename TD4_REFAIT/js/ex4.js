"use strict"

export function init() {
  $(".menu").on("click",affichermenu);
}

function affichermenu(e) {
  $(e.target).parent().siblings().find('ul').hide(500);
  $(e.target).next().slideToggle();
}
