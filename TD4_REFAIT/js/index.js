"use strict"
import * as ex1 from './ex1.js';
import * as ex2 from './ex2.js';
import * as ex3 from './ex3.js';
import * as ex4 from './ex4.js';
import * as ex5 from './ex5.js';

$(document).ready( () => {

  ex1.init();
  ex2.init();
  ex3.init();
  ex4.init();
  ex5.init();

})
