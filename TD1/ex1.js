"use strict"

/* exercice 1.1 */

let min = (nb1, nb2) => {
	return nb1 < nb2 ? nb1 : nb2
}

let max = (nb1, nb2) => {
	return nb1 > nb2 ? nb1 : nb2
}

/* exercice 1.2 */

let affiche = () => {
	for(let i=1; i<=100; i++) {
		if(i%3 === 0) {
			console.log(i + " woueee")
		} else if (i%5 ===0) {
			console.log(i + " yoooo")
		} else if (i===73) {
			console.log(i + " Biinnngooo")
		} else {
			console.log(i)
		}
	}
}

/* exercice 1.3 */

let power = (nombre, puissance) => {
	let resultat = 1

	for(let i=1; i<=puissance; i++) {
		resultat = resultat*nombre
	}

	return resultat
}

/* exercice 1.4 */

let power_recursif = (x, n) => {
	if(n===0) {
		return 1
	} else {
		return x * power_recursif(x, n-1)
	}
}

