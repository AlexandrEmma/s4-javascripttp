"use strict"

/* exercice 2.1 */

let creerMultiplicateur = (n) => {
  return (x) => { return x*n ;} ;
}
let multi = creerMultiplicateur(4);

/* exercice 2.2 */

let creerSequence = (init,step) => {
  return () => (init++)*step;
}
let incre = creerSequence(0,10);

/* exercice 2.3 */

let fibonacci = (x,y) => {
  return () => {
    let tmp = x;
    x = y;
    y = tmp + y;
    return y;
  };
}
let fibonac = fibonacci(1,1);

/* exercice 2.4 */

let creerMultiplicateurv2 = (n,x) => {
  if(x === undefined) {
    return (x) => x*n;
  } else {
    return n*x;
  }
}
let multiv2test0 = creerMultiplicateurv2(5);
let multiv2test1 = creerMultiplicateurv2(5,4);

/* exercice 2.5 */

let powerv2 = (n, x) => {
  if(x === undefined) {
    return function Funct(x) {
      if(n===0) {
        return 1;
      } else {
        n -= 1;
        return x* Funct(x);
      }
    }
  } else {
  	let resultat = 1
  	for(let i=1; i<=n; i++) {
  		resultat = resultat*x
  	}
  	return resultat;
  }
}
let powerv0test0 = powerv2(3);

/* exercice 2.6 */

let formatter = (num) => {
  return (p) => { return (num++)+" : "+p; };
}
var format = formatter(10);

/* exercice 2.7 */

let write = (m) => console.log(m);

let writeAlert = (m) => alert(m);


/* exercice 2.8 */

let logger = (f , w) => {
  return (m) => {
    let forma = f(5);
    var apresformat = forma(m);
    w(apresformat);
  }
}
let log = logger(formatter,write);
