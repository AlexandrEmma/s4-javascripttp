"use strict"

/* exercices */

window.addEventListener("load", () => {

  let logClick = () => { console.log("click"); compteurclick++;  };

  let updateButtonClass = () => {
    element.classList.toggle("c1");
    element.classList.toggle("c2"); }

  let insereritem = () => {
    let ul = document.querySelector("#ex2");
    let nli = document.createElement("li");
    let txt = document.createTextNode("click bouton : " + compteurclick);
    nli.appendChild(txt);
    ul.appendChild(nli);
  }

  let clearitem = () => {
    let ul = document.querySelector("#ex2");
    while (ul.hasChildNodes()) {
      ul.removeChild(ul.lastChild);
    }
  }

  let incrInputValue = () => {
    let input = document.querySelector("#ex4i");
    input.setAttribute("value",input.value++);
  }

  let compteurclick = 0;

  var element = document.querySelector("#ex1");
  element.addEventListener("click", updateButtonClass);
  element.addEventListener("click", logClick);
  element.addEventListener("click", insereritem);

  var bouton2 = document.querySelector("#ex3");
  bouton2.addEventListener("click", clearitem);

  var bouton3 = document.querySelector("#ex4b");
  bouton3.addEventListener("click", incrInputValue);
});
