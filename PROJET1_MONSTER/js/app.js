//module qui contient les fonctions de déroulement de l'application

import actions from "./actions.js";
import {log, displayStatus} from "./ui.js";

let appModule = ( function() {
  //variables privées
  var b6 = document.querySelector("#b6");
  var b2 = document.querySelector("#b2");
  var b3 = document.querySelector("#b3");
  var b7 = document.querySelector("#b7");
  var b4 = document.querySelector("#b4");
  var b5 = document.querySelector("#b5");
  var b1 = document.querySelector("#b1");
  var k = document.querySelector("#k");

  //méthode getshow
  function get() {
    let objetdom = {
      boutonshow : b6,
      boutonrun : b2,
      boutonfight : b3,
      boutonwork : b7,
      boutonsleep : b4,
      boutoneat : b5,
      boutonewlife : b1,
      boutonkill : k
    }
    return objetdom;
  };

  //partie public retournée
  return {
    get : get
  }

}) ();

export function start() {
  //initialise le monstre
  actions.init("Corentin",50,100,true);

  //on récupère un objet qui correspond à l'état actuel du monstre
  let etatcourant = actions.get();

  //on récupère un objet qui correspond aux différents objets du dom recevant des évènements
  let dom = appModule.get();

  //fonction log et display

  displayStatus(etatcourant);

  /* on récupère le bouton et on affiche l'état courant du monstre en utilisant la fonction show qui affiche
   * les variables de l'état du monstre lors d'un click sur le bouton "b6"
   */
  let show = () => {
     var res = "";
     for (var p in etatcourant) {
       res += p+" : "+etatcourant[p]+"\n";
     }

     alert(res);
  }
  dom.boutonshow.addEventListener("click",show);

  //fonction qui actualise l'état
  let actualiser = () => {
    etatcourant = actions.get();
    if(etatcourant.life == 0) {
      log("Le monstre est mort");
    }
    displayStatus(etatcourant);

  }

  /*si le monstre est vivant et réveillé il peut faire des actions
   *le monstre peut courir
   */
  let run = () => {
    if(etatcourant.life > 0 && etatcourant.awake==true) {
      actions.run();
      log("Le monstre court\n");
      actualiser();
    }
  }
  dom.boutonrun.addEventListener("click",run)

  //le monstre peut se battre
  let fight = () => {
    if(etatcourant.life >= 3 && etatcourant.awake==true) {
      actions.fight();
      log("Le monstre se bat\n");
      actualiser();
    }
  }
  dom.boutonfight.addEventListener("click",fight)

  //le monstre travaille
  let work = () => {
    if(etatcourant.life >= 1 && etatcourant.awake==true) {
      actions.work();
      log("Le monstre travaille\n");
      actualiser();
    }
  }
  dom.boutonwork.addEventListener("click",work)

  //le monstre mange
  let eat = () => {
    if(etatcourant.money >= 3 && etatcourant.awake==true && etatcourant.life > 0) {
      actions.eat();
      log("Le monstre mange\n");
      actualiser();
    }
  }
  dom.boutoneat.addEventListener("click",eat)

  //le monstre dort
  let sleep = () => {
    if(etatcourant.awake==true && etatcourant.life > 0) {
      actions.sleep();
      log("Le monstre dort\n");
      actualiser();
      setTimeout(() => {actualiser();log("Le monstre se réveille")},10000);
    }
  }
  dom.boutonsleep.addEventListener("click",sleep)

  //fonction qui toute les 12s retire 1 point de vie au monstre et choisit une action au hasard
  let auto = () => {
    if(etatcourant.life>0) {
      actions.auto();
      actualiser();
      //on veut choisir une action au hasard
      let numero = Math.floor(Math.random() * (5 - 1) + 1);
      switch(numero) {
        case 1:
          run();
          break;
        case 2:
          fight();
          break;
        case 3:
          work();
          break;
        case 4:
          eat();
          break;
        case 5:
          sleep();
          break;
      }
    }
  }
  setInterval(auto,12000);

  //fonction qui tue le monstre
  let kill = () => {
    actions.kill();
    log("Le monstre est tué\n");
    actualiser();
  }
  dom.boutonkill.addEventListener("click",kill)

  //fonction qui redonne vie au monstre
  let newlife = () => {
    actions.newlife();
    log("Le monstre se réanime\n");
    actualiser();
  }
  dom.boutonewlife.addEventListener("click",newlife)
}
