
export function log(message) {

  var actionbox = document.querySelector("#actionbox");
  let p = document.createElement("P");
  let texte = document.createTextNode(message);

  p.appendChild(texte);
  actionbox.prepend(p); //insère comme premier fils

}

export function displayStatus(etat) {

  //on sélectionne la liste
  var status = document.querySelector("#status");
  let etatmonstre = status.childNodes;

  //changement de la valeur de life
  let texte = document.createTextNode("life:"+etat.life);
  etatmonstre[1].replaceChild(texte,etatmonstre[1].firstChild)

  //changement de la valeur de money
  let texte2 = document.createTextNode("money:"+etat.money);
  etatmonstre[2].replaceChild(texte2,etatmonstre[2].firstChild)

  //changement de la valeur de awake
  let texte3 = document.createTextNode("awake:"+etat.awake);
  etatmonstre[3].replaceChild(texte3,etatmonstre[3].firstChild)

  //faire varier la couleur de la voite #monster en fonction des points de vie du monstre
  var vie = document.querySelector("#monster");
  if(etat.life < 5) {
    vie.style.backgroundColor = 'red';
  } else if(etat.life < 10) {
    vie.style.backgroundColor = 'orange';
  } else if(etat.life < 15) {
    vie.style.backgroundColor = 'blue';
  } else if(etat.life > 15) {
    vie.style.backgroundColor = 'green';
  }

  //faire varier la bordure de la boite #monster en fonction de la quantité d'argent du monstre
  var money = document.querySelector(".monster");
  money.style.height = etat.money+"px";

}
