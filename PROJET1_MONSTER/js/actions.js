//module qui contient l'état et les actions que peut réaliser le monstre


let actionsModule = ( function() {
  //variables privées
  let name;
  let life;
  let money;
  let awake;  //reveillé ou non

  //retourne l'état courant du monstre sous forme d'objet
  function get() {
    let etatcourant = {
      name : actionsModule.name,
      life : actionsModule.life,
      money : actionsModule.money,
      awake : actionsModule.awake
    };
    return etatcourant;
  };

  //initialise l'état du monstre avec les valeurs reçues en paramètres
  function init(n,l,m,a) {
    actionsModule.name = n;
    actionsModule.life = l;
    actionsModule.money = m;
    actionsModule.awake = a;
  };

  function run() {
    actionsModule.life -= 1;
  }

  function fight() {
    actionsModule.life -= 3;
  }

  function work() {
    actionsModule.life -= 1;
    actionsModule.money += 2;
  }

  function eat() {
    actionsModule.money -= 3;
    actionsModule.life += 2;
  }

  function sleep() {
    actionsModule.awake = false;
    let aprestemps = () => {
      actionsModule.life += 1;
      actionsModule.awake = true;
    }
    setTimeout(aprestemps, 10000);
  }

  function auto() {
    actionsModule.life -= 1;
  }

  function kill() {
    actionsModule.life = 0;
    actionsModule.money = 0;
    actionsModule.awake = null;
  }

  function newlife() {
    actionsModule.life = 30;
    actionsModule.money = 10;
    actionsModule.awake = true;
  }

  //Partie de l'objet actionsModule public
  return {
    get : get,
    init : init,
    run : run,
    fight : fight,
    work : work,
    eat : eat,
    sleep : sleep,
    auto : auto,
    kill : kill,
    newlife : newlife
  }

}) ();

//fonction exportée
export default {
  get : actionsModule.get,
  init : actionsModule.init,
  run : actionsModule.run,
  fight : actionsModule.fight,
  work : actionsModule.work,
  eat : actionsModule.eat,
  sleep : actionsModule.sleep,
  auto : actionsModule.auto,
  kill : actionsModule.kill,
  newlife : actionsModule.newlife
}
