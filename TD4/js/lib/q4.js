let toggleMenu = function(title) {
	$(title).next().toggle(500);
};

let hideSiblingsMenus = function(title) {

	$(title).parent().siblings().find('ul').hide(500);
};

let init = function () {
	$(".sub").on('click', function(e) {
		hideSiblingsMenus(e.target);
		toggleMenu(e.target);
	});
};

export default {
	init:init
}
