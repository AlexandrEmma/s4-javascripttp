let toggleText = function (e) {
	$(e.target).toggleClass("hop").toggleClass("zap");
};

let init = function() {
	$('#changingText').click(toggleText);
};

export default  {
	init : init
};
