let hover = function (e) {
    $(e.target).addClass('boite_large');
};

let reset = function(e) {
    $(e.target).removeClass('boite_large');
};

let addBox = function () {
    let div$ = $('<div class="boite_orange">5</div>');
    $("section#square").append(div$);
    div$.mouseover(hover);
    div$.click(reset);
};

let toggleBoxes = function() {
    $("#square >div").toggleClass('boite_orange');
    $("#square >div").toggleClass('boite_verte');
};

let fadeBoxes = function () {
    $('#square>div').slideToggle(500);
};

let init = function() {
    $('#square > div').mouseover(hover);
    $('#square > div').click(reset);
    $('button#one').click(addBox);
    $('button#two').click(toggleBoxes);
    $('button#three').click(fadeBoxes);
};

export default {
    init:init
}
