
let show = function (m) {
    let modal = $(m);
    modal.show(300);
    modal.find('.modal_close').click(function() {
        modal.hide(300);
    })
};

let init = function( id ) {
    $("#four").click(function() {
        show(id);
    });
};

export default {
    init:init
}
