let setContent = function(text) {
	$('#slider > .slidingContent').html(text);
};

let slide = function() {
	$('#slider > .slidingContent').slideToggle(600);
};

let init = function(text=null) {
	if (text !== null){
		setContent(text);
	}
	$('#showContent').click(slide);
}

export default  {
	init: init,
	setContent: setContent
};
