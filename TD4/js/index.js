import q1 from './lib/q1.js' ;
import q2 from './lib/q2.js' ;
import q3 from './lib/q3.js' ;
import q4 from './lib/q4.js' ;
import q5 from './lib/q5.js' ;

$(document).ready(function() {

    q1.init();
    q2.init();
    q3.init('<strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, ipsam.</strong>');
    q4.init();
    q5.init("#modal1");

});
