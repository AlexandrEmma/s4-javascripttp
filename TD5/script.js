
// Question 1
let show_books = function(e) {
	let books = JSON.parse(e.target.responseText)
	console.log(books)
}

let send = function(url, c) {
	let xhr = new XMLHttpRequest()
	xhr.open('GET', url)
	xhr.addEventListener('load', c)
	xhr.send()
}

//send('https://www.anapioficeandfire.com/api/books', show_books)

let show_book_new = function(n) {
	send('https://www.anapioficeandfire.com/api/books/' + n, books_details)
}

let books_details = function(e) {
	let book = JSON.parse(e.target.responseText)
	console.log('titre:' + book.name + ', nombre de pages:' + book.numberOfPages)
	send(book.characters[0], show_iaf_character)
}

let show_iaf_character = function(e) {
	let char = JSON.parse(e.target.responseText)
	console.log(char.url)
	console.log(char.name)
	console.log(char.gender)
}

show_book_new(2)
